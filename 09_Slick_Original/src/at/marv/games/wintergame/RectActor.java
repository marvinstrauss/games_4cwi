package at.marv.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectActor {
	private double x, y;
	private direction dir;

	public enum direction {
		right, left, up, down
	}

	public RectActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
		this.dir = direction.right;
	}

	public void update(GameContainer gc, int delta) {
		if(this.dir == direction.right) {
			this.x ++;
			if(this.x == 700) {
				this.dir = direction.down;
			}
		}
		else if (this.dir == direction.down) {
			this.y ++;
			if(this.y == 500) {
			this.dir = direction.left;
			}
			
		}
		else if (this.dir == direction.left) {
			this.x --;
			if(this.x == 100) {
			this.dir = direction.up;
			}
			
		}
		else if (this.dir == direction.up) {
			this.y --;
			if(this.y == 100) {
			this.dir = direction.right;
			}
		}

	}

	public void render(Graphics graphics) {
		graphics.drawRect((float) this.x, (float) this.y, 70, 70);
	}
}
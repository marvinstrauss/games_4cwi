package at.marv.games.wintergame;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.marv.games.wintergame.OvalActor.direction;


public class Snowflakes {
	private double x, y;
	private float width;
	private float height;
	private float speed;
	private Size siz ;
	Random randX;
	Random randY;

	public enum Size {
		small, medium, big 
	}

	public Snowflakes (Size siz) {
		super();
		this.randX = new Random();
		this.randY = new Random();
		this.x = randX.nextInt(799) + 1;
		this.y = randY.nextInt(600) + 0;
		this.y *= -1;
		this.siz = siz;
	
	if(this.siz == Size.small) {
		this.width = 10;
		this.height = 10;
		this.speed = 0.4f;
	}
	if(this.siz == Size.medium) {
		this.width = 15;
		this.height = 15;
		this.speed = 0.5f;
	}
	if(this.siz == Size.big) {
		this.width = 25;
		this.height = 25;
		this.speed = 0.6f;
	}
	}
	public void update(GameContainer gc, int delta) {
		if (this.y <= 600) {
			this.y += delta*this.speed;
		}
		else if (this.y >= 600) {
			this.x = randX.nextInt(799) + 1;
			this.y = -20;
		}
		
	}

	public void render(Graphics graphics) {
		graphics.fillOval((float) this.x, (float) this.y, this.width, this.height);
	}
}

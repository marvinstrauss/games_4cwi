package at.marv.games.wintergame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame {
	private CircleActor ca1;
	private RectActor ra1;
	private OvalActor oa1;
	private List<Snowflakes> Snowflake; 
	private ShootingStar ss1;
	
	
	
	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		//gezeichnet
		this.ca1.render(graphics);
		this.ra1.render(graphics);
		this.oa1.render(graphics);
		this.ss1.render(graphics);
		for(Snowflakes sf : this.Snowflake) {
			sf.render(graphics);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// 1 mal aufgerufen
		this.ca1 = new CircleActor(400,400);
		this.ra1 = new RectActor(100,100);
		this.oa1 = new OvalActor(400,400);
		this.ss1 = new ShootingStar(100,100);
		this.Snowflake = new ArrayList<>();
		
		for (int i = 0; i < 30; i++) {
			this.Snowflake.add(new Snowflakes(Snowflakes.Size.small));	
		}
		for (int i = 0; i < 30; i++) {
			this.Snowflake.add(new Snowflakes(Snowflakes.Size.medium));	
		}
		for (int i = 0; i < 30; i++) {
			this.Snowflake.add(new Snowflakes(Snowflakes.Size.big));	
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		//laufend aufgerufen, delta = zeit seit dem letzten aufruf
		this.ca1.update(gc, delta);
		this.ra1.update(gc, delta);
		this.oa1.update(gc, delta);
		this.ss1.update(gc, delta);
		for(Snowflakes sf : this.Snowflake) {
			sf.update(gc,delta);
		}
		}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}

package at.marv.games.wintergame;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class ShootingStar {
	private float x, y;
	private float counter;
	public float randomNumb;
	private int timeHere;
	private boolean isVisible;

	public ShootingStar(float x, float y) {
		super();
		this.x = x;
		this.y = y;
		this.timeHere = 0;
		this.isVisible = false;
	}

	private void setRandomNumber() {
		this.randomNumb = RandomNumber(7) + 6;
	}

	private int RandomNumber(int range) {
		Random r = new Random();
		return r.nextInt(range);
	}

	public void update(GameContainer gc, int delta) {
	

		this.timeHere += delta;

		if (this.timeHere > (this.randomNumb * 1000)
				&& this.timeHere< (this.randomNumb * 1000 + 3500)) {

			this.isVisible = true;
			this.x = (float) (this.x + delta * 0.4);
			this.y = (float) (0.0005 * this.x * this.x + -0.001 * this.x + 300);

		} else if (timeHere> (this.randomNumb * 1000 + 3499)) {

			this.isVisible = false;
			this.timeHere = 0;
			this.x = -20;
			this.y = 300;
			setRandomNumber();

		}
	}

	public void render(Graphics graphics) {
  graphics.setColor(Color.yellow);
  if (this.isVisible == true) {
   graphics.fillOval(this.x, this.y, 20, 20, 6);
  }
  graphics.setColor(Color.white);

 }
}

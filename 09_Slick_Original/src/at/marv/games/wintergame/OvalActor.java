package at.marv.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;


public class OvalActor {
	private double x, y;
	private direction dir;

	public enum direction {
		right, left 
	}

	public OvalActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
		this.dir = direction.right;
	}

	public void update(GameContainer gc, int delta) {
		
		if(this.dir == direction.right) {
			this.x ++;
			if(this.x == 700) {
				this.dir = direction.left;
			}
		}
		
		else if(this.dir == direction.left) {
			this.x --;
			if(this.x == 50) {
				this.dir = direction.right;
			}
		}

	}

	public void render(Graphics graphics) {
		graphics.drawOval((float) this.x, (float) this.y, 90, 70);
	}
}